source_dir="ffbox"
destination="ffbox.deb"

if [ -e "$destination" ]; then
    rm "$destination"
fi

chmod -R 755 "$source_dir/DEBIAN/"

dpkg-deb --build "$source_dir/"
if [ "$source_dir.deb" != "$destination" ]; then
    mv "$source_dir.deb" "$destination"
fi
chmod 755 "$destination"
