#!/bin/bash

## Variables d'environnements
work_profile="$1"

## Lancement de firefox
echo "strict" > "${work_profile}/.ffbox-type" #juste à titre indicatif

firejail --name="ffbox-strict" \
    --read-only=/bin --read-only=/proc --read-only=/usr --read-only=/var \
    --blacklist=/boot --blacklist=/media --blacklist=/mnt --blacklist=/opt --blacklist=/sbin --blacklist=/srv --blacklist=/sys \
    --private=${work_profile} --private-etc=fonts,resolv.conf --private-dev \
    firefox -no-remote

#Supression des données
rm -R "${work_profile}"
