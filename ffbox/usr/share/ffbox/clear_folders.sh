#!/bin/sh

#Variable d'environnement
work_dir="$HOME/.cache/ffbox"
profiles_names="ffbox_"
shared_name="$(basename $(cat $HOME/.config/ffbox/shared_directory.txt))"

check_no_active_session() {
    if [ "$(firejail --list | grep -E 'firefoxsecure-amnesic|firefoxsecure-classic|firefoxsecure-config' | wc -l)" -gt 0 ]; then
        message="<b>Des instances de FFBox sont actives...</b>"
        message="${message}\nVeuillez les arrêtez avant de faire un nettoyage.\n"
        zenity --error --width 400 \
            --window-icon="ffbox-config" \
            --title "Erreur sessions FFBox actives" \
            --text "${message}"
        exit 1
    fi
}

umount_directories() {
    #tentative de démontage des dossiers du profiles
    files_dir="${1}/.files/"
    if [ -e "${files_dir}" ]; then
        for i in "$(ls ${files_dir})"; do
            if [ -d "${files_dir}${i}" ]; then
                umount_directory "${files_dir}${i}"
            fi
        done
    fi
    umount_directory "${1}${shared_name}"
}
umount_directory() {
    #tant qu'il n'y a pas de message d'erreur, ont tente de démonter
    while [ "$(fusermount -u ${1} 2>&1)" == "" ]; do
        sleep 0.1
    done
}

check_no_active_session

#parcour et supprime les sessions
profiles="$(ls ${work_dir} | grep ${profiles_names})"
for rep in ${profiles}; do
    umount_directories "$work_dir/${rep}"
    rm -R "$work_dir/$rep"
done
