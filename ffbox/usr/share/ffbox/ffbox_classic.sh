#!/bin/bash

## Variables d'environnements
work_profile="$1"
shared_directory="$(cat $HOME/.config/ffbox/shared_directory.txt)"
shared_directory="${work_profile}/$(basename $shared_directory)"



available_files() {
    number_files="$(ls ${work_profile}/.files/ 2>/dev/null | wc -l)"
    if [ "${number_files}" -eq 0 ]; then
        echo "nothing"
    elif [ "${number_files}" -eq 1 ]; then
        echo "single"
    elif [ "${number_files}" -gt 1 ]; then
        echo "multiple"
    else
        echo "nothing"
    fi
}

umount_directories() {
    #tentative de démontage des dossiers du profiles
    files_dir="${work_profile}/.files/"
    if [ -e "${files_dir}" ]; then
        for i in "$(ls ${files_dir})"; do
            if [ -d "${files_dir}${i}" ]; then
                umount_directory "${files_dir}${i}"
            fi
        done
    fi
    umount_directory "${shared_directory}"
}
umount_directory() {
    #tant qu'il n'y a pas de message d'erreur, ont tente de démonter
    while [ "$(fusermount -u ${1} 2>&1)" == "" ]; do
        sleep 0.1
    done
}



## Lancement de firefox
echo "classic" > "$work_profile/.ffbox-type" #juste à titre indicatif
do_open_files="$(available_files)"

#démarrage simple de firefox
if [ "${do_open_files}" == "nothing" ]; then
    firejail --private=${work_profile} --name="ffbox-classic" --private-dev firefox -no-remote

#démarrage avec 1 fichier
elif [ "${do_open_files}" == "single" ]; then
    filename="$HOME/.files/$(ls ${work_profile}/.files)"
    firejail --private=${work_profile} --name="ffbox-classic" --private-dev firefox "${filename}" -no-remote

#démarrage avec plusieurs fichier
elif [ "${do_open_files}" == "multiple" ]; then
    firejail --private=${work_profile} --name="ffbox-classic" --private-dev firefox "$HOME/.files/" -no-remote

else
    echo "Erreur : passage en démarrage classique de ffbox."
    firejail --private=${work_profile} --name="ffbox-classic" --private-dev firefox -no-remote
fi



## Arrêt de firefox

#démontage des répertoires partagés
umount_directories

#suppresion du profil
rm -R "${work_profile}"
