#!/bin/bash

#pour reconnaitre le type de firefox utilisé :
#strict : il n'y a pas de dossier Téléchargement.
#classic : il y a seulement le dossier Téléchargement.
#config : il y a le fichier "FFBox-configurator" à la racine du profil.

## Variables d'environnements
work_dir="$HOME/.cache/ffbox"
shared_directory="$(cat $HOME/.config/ffbox/shared_directory.txt)"
shared_name="$(basename ${shared_directory})"

remove_profile() {
    umount_directories "${1}"
    rm -R "${1}"
}

umount_directories() {
    files_dir="${1}/.files/"
    if [ -e "$files_dir" ]; then
        for i in "$(ls ${files_dir})"; do
            if [ -d "${files_dir}${i}" ]; then
                umount_directory "${files_dir}${i}"
            fi
        done
    fi
    umount_directory "${1}/${shared_name}"
}
umount_directory() {
    #tant qu'il n'y a pas de message d'erreur, ont tente de démonter
    while [ "$(fusermount -u ${1} 2>&1)" == "" ]; do
        sleep 0.1
    done
}

mount_download_dir() {
    target="${1}/$shared_name"
    echo -n "Try to mount ${shared_directory} on ${target}..."
    mkdir -p "${target}"
    bindfs "${shared_directory}" "${target}" && echo "ok" || error_mouting_download_dir "${1}"
}
error_mouting_download_dir() {
    message="<b>Erreur, le montage du répertoire de téléchargement a échoué.</b>"
    message="$message\nrepertoire: ${1}"
    zenity --error --width 400 \
        --window-icon="ffbox-config" \
        --title "Erreur répertoire partagé" \
        --text "${message}"
}

start_ffbox_classic() {
    umount_directories "${1}"
    mount_download_dir "${1}" #cant remount other directory without paths
    echo "New FFBox-Classic instance use \"$1\" as profile directory."
    /usr/share/ffbox/ffbox_classic.sh "${1}" || error_starting "${1}"
}
start_ffbox_strict() {
    echo "New FFBox-Strict instance use \"$1\" as profile directory."
    /usr/share/ffbox/ffbox_strict.sh "${1}" || error_starting "${1}"
}



for i in `ls ${work_dir}`; do
    #récupérer le chemin absolue du profil existant
    work_profile="${work_dir}/${i}"
    #si le fichier .ffbox-type n'existe pas à la racine du profil, on supprime le profil
    if [ ! -e "${work_profile}/.ffbox-type" ]; then
        remove_profile "${work_profile}"
    else
        #détecter le type de firefox qui utilisait ce profil
        work_type="$(cat ${work_profile}/.ffbox-type)"
        #selon le type, démarrer ffbox ou supprimer le profil
        if [ ${work_type} == "config" ]; then
            remove_profile "${work_profile}"
        elif [ ${work_type} == "classic" ]; then
            start_ffbox_classic "${work_profile}" &
        elif [ ${work_type} == "strict" ]; then
            start_ffbox_strict "${work_profile}" &
        fi
    fi
done
