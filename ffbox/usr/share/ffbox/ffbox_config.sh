#!/bin/bash

## Variables d'environnements
work_profile="${1}"
default_profile="$HOME/.config/ffbox/default_profile"
shared_directory="$(cat $HOME/.config/ffbox/shared_directory.txt)"
shared_directory="${work_profile}/$(basename $shared_directory)"

check_want_start_configurator() {
    #on affiche un message avec confirmation qu'il faut bien démarrer le navigateur de configuration
    message="<b>ATTENTION : Le Firefox de configuration n'est PAS destiné à la navigation Web.</b>

    Celui-ci est destiné uniquement à l'ajout de configurations internes :
	    - Extensions
	    - Personnalisations

    <span color=\"red\">Toute recherche sur le web à partir de ce navigateur serait archivée et serait
    sauvegardée dans les sessions suivantes.
    <b>Ces données enregistrées pourraient permettre d'itenfier vos sessions sur Internet.</b></span>

    <b>Voulez-vous continuer et personnaliser votre Firefox ?</b>
    "
    zenity --question --width 610 \
        --window-icon="/usr/share/icons/ffbox-config.png" \
        --title "Confirmez le démarrage de FFBox-Configuration" \
        --text "${message}"
    #arrêt du programme si l'utilisateur annule.
    if [ "$?" -ne 0 ]; then
        echo "Arrêt de FFBox-Config par l'utilisateur"
        notify-send -t 4600 -i "ffbox-config" \
            "Arrêt de la session de configuration pour FFBox !"
        umount_directories
        rm -R "${work_profile}"
        exit 0
    fi
}

umount_directories() {
    #tentative de démontage des dossiers du profiles
    files_dir="${work_profile}/.files/"
    if [ -e "${files_dir}" ]; then
        for i in "$(ls ${files_dir})"; do
            if [ -d "${files_dir}${i}" ]; then
                umount_directory "${files_dir}${i}"
            fi
        done
    fi
    umount_directory "${shared_directory}"
}
umount_directory() {
    #tant qu'il n'y a pas de message d'erreur, ont tente de démonter
    while [ "$(fusermount -u ${1} 2>&1)" == "" ]; do
        sleep 0.1
    done
}

clean_default_profile() {
    #Suppression de dossiers d'environnement
    rm -R "${default_profile}/Téléchargements"
    rm -R "${default_profile}/Downloads"
    rm -R "${default_profile}/Desktop"
    rm -R "${default_profile}/.cache"
    rm -R "${default_profile}/.config"
    rm -R "${default_profile}/.local"
    rm -R "${default_profile}/.pki"
    rm -R "${default_profile}/.bash_rc"
    rm -R "${default_profile}/.Xauthority"
    rm -R "${default_profile}/.files"
    rm "${default_profile}/.ffbox-type"
    #Suppression de dossiers de configuration de firefox
    rm -R "${default_profile}/.mozilla/firefox/Crash Reports"
    #on enlève des données du profil firefox
    rm "${default_profile}/.mozilla/firefox/"*".default"*"/saved-telemetry-pings/"
    rm -R "${default_profile}/.mozilla/firefox/"*".default"*"/storage/temporary/"
    rm -R "${default_profile}/.mozilla/firefox/"*".default"*"/storage/to-be-removed/"
}

## Vérification et demande de confirmation
check_want_start_configurator



## Lancement de firefox
echo "config" > "${work_profile}/.ffbox-type" #juste à titre indicatif

#création d'un lien du fichier d'information config.html pour qu'il soit ouvert
mkdir "${work_profile}/.files"
ln "/usr/share/ffbox/webpage/config.html" "${work_profile}/.files/config.html" || cp "/usr/share/ffbox/webpage/config.html" "${work_profile}/.files/config.html"
ln "/usr/share/ffbox/webpage/warning.png" "${work_profile}/.files/warning.png" || cp "/usr/share/ffbox/webpage/warning.png" "${work_profile}/.files/warning.png"

#démarrage de firefox
firejail --private=${work_profile} --name="ffbox-config" --private-dev firefox "$HOME/.files/config.html" -no-remote



## Fermeture et sauvegarde de firefox

#démontage des répertoires partagés
umount_directories

#on vérifie que l'utilisateur veut bien enregistrer la configuration
zenity --question --width 400 \
    --window-icon="/usr/share/icons/ffbox-config.png" \
    --title "Enregistrer les informations ?" \
    --text "<b>Voulez-vous enregistrer la configuration effectué comme configuration par défaut pour FFBox Classic et Strict ?</b>"
if [ "$?" -ne 0 ]; then
    notify-send -t 4600 -i "ffbox-config" "Fermeture de FFBox-Config" \
        "Fermeture de FFBox-Configuration sans enregistrement de la configuration."
    rm -R "${work_profile}"
    exit 0
fi

#on supprime le profile par défaut
rm -R "${default_profile}"
#on déplace le profile courant à sa place
mv "${work_profile}" "${default_profile}"
#quelques nettoyages
clean_default_profile



#notification pour la fin de la sauvegarde de la conf
message="<b>Fermeture et sauvegarde de la configuration de FFBox effectué !\n"
message="${message}\nFFbox prendra en compte cette configuration dans les prochaines sessions.</b>\n"
message="${message}\nSi besoin, vous pouvez redémarrer les sessions en cours pour que les changements soit immédiatement pris en compte (une sauvegarde manuelle des  pages ouvertes sera nécessaire)."
zenity --info --width 460 \
    --window-icon="/usr/share/icons/ffbox-config.png" \
    --title="Configuration de FFBox terminée" \
    --text="${message}"
