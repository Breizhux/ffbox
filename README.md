# FFBOX

**FFBox for Firefox in a Box.**
This program is a script that launches the **Firefox browser in a sandbox thanks to the Firejail** program. But rather than simply launching the program in the sandbox, **the script launches Firefox with a temporary profile**. In this way, you can launch as many Firefox browsers as you like, **totally independent of each other**.
What's more, every time a session is closed, any data that may have been stored in it is completely deleted. **At the end of the session, the entire temporary profile is deleted.**

**Warning**: Firefox consumes a lot of RAM memory... You need 1GB of RAM per Firefox session. So don't launch too many different sessions at the same time.

## There are 2 types of Firefox

Two different FFBoxes are required for this project to run smoothly.

### FFBox Classic :

**This is the FFBox to be used on a daily basis**.

**It can only access one directory in your user folder**. This is where you can upload files. To send files from your documents, you'll need to manually copy them to the download folder before you can send them with the browser.
Please note that **no configuration you make on such a session will be persistent**. If you add an extension, or change a Firefox setting, these changes will be permanently lost when you close the session.

### FFBox Config :

**This is the FFBox for persistent configurations **.
<mark>**This FFBox must never be used to browse the Internet. It is only used to provide FFBox configurations.**</mark>

<mark>**Any traces you leave there could render the protections offered by this browser useless!**</mark>

When you open it, a page will guide you through its configuration.

Any changes you make will be taken into account when you start FFBox Classic in the future.


## Installation

## On Debian-type systems :

1. Download the debian package:
   - Go to the [release](https://gitlab.com/Breizhux/ffbox/-/releases) section.
   - Download the file ending with `.deb`.

2. Install the downloaded file:
   - Open the terminal in the directory where the previously downloaded `.deb` is located.
   - Type the following command: `sudo apt install ./ffbox*.deb`.
   - Then, you can remove `.deb` file with `rm ffbox*.deb` command.
