# FFBOX

**FFBox pour Firefox in a Box.**
Ce programme est un script qui lance le navigateur **Firefox dans un bac à sable grâce au programme Firejail**. Mais plus que de simplement lancer le programme dans le bac à sable, **le script se charge de lancer Firefox avec un profil temporaire**. Ainsi, il est possible de lancer autant de Firefox que vous le souhaitez de manière **totalement indépendant les uns des autres**.
De plus, à chaque fermeture de session, toutes les données qui aurait pu y être stocké sont totalement supprimées. **À la fin de la session, c'est tout le profil temporaire qui est supprimé.**

**Attention** : Notez que Firefox consomme pas mal de mémoire RAM… Il faut compter 1Go de RAM par session Firefox. Ne lancez donc pas trop de session différente en même temps.

## Il y a 2 types de Firefox

Deux FFBox différents sont nécessaires pour le bon fonctionnement de ce projet.

### FFBox Classic :

**Il s'agit du FFBox à utiliser quotidiennement.**

**Il ne peut accèder qu'à un seul répertoire de votre dossier utilisateur**. De cette manière vous pourrez y télécharger des fichiers. Pour envoyer des fichiers depuis vos documents, il sera nécessaire de le copier manuellement vers le dossier de téléchargement avant de pouvoir l'envoyer avec le navigateur.
Notez qu'**aucune configuration que vous pourrez effectuer sur une telle session ne sera persitente**. Si vous ajoutez une extension, ou modifier un paramètre de Firefox, ces modifications seront définitivement perdus lorsque vous fermerez la session.

### FFBox Config :

**Il s'agit du FFBox permettant de réaliser des configurations persistente.**

<mark>**Ce FFBox ne doit en aucun cas être utilisé pour naviguer sur Internet. Il ne sert qu'à apporter des configurations pour FFBox. Toutes traces que vous y laisserez pourrait rendre inutile les protections offertes par ce navigateur !**</mark>

Lorsque vous l'ouvrirez, une page vous guidera dans sa configuration.

Toutes les modifications effectuées seront prises en compte lors des futures démarrages de FFBox Classic.


## Installation

### Sur les systèmes de type Debian :

1. Télécharger le paquet debian :
   - Rendez vous dans la section [release](https://gitlab.com/Breizhux/ffbox/-/releases)
   - Télécharger le fichier se terminant par `.deb`.

2. Installer le fichier téléchargé :
   - Ouvrir le terminal dans le répertoire où se trouve le `.deb` précédemment téléchargé.
   - Taper la commande suivante : `sudo apt install ./ffbox*.deb`
   - Ensuite, vous pouvez supprimer le fichier `.deb` avec la commande `rm ffbox*.deb`.
